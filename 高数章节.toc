\contentsline {chapter}{版权许可}{i}{chapter*.1}
\contentsline {chapter}{前言}{i}{chapter*.2}
\contentsline {chapter}{\numberline {1}上册}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}第一章\hskip 1em\relax 函数与极限}{1}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}习题1-1\hskip 1em\relax 映射与函数}{1}{subsection.1.1.1}
\contentsline {paragraph}{(A)1\hskip 1em\relax 求下列函数的定义域}{1}{section*.4}
\contentsline {subparagraph}{$\displaystyle (4)\hskip 1em\relax y = \frac {x}{\qopname \relax o{tan}x}$ \\}{1}{section*.5}
\contentsline {subparagraph}{$\displaystyle (5)\hskip 1em\relax y = lg(1 - \qopname \relax o{cos}x)$ \\}{2}{section*.6}
\contentsline {subparagraph}{$\displaystyle (6)\hskip 1em\relax y = \sqrt {3 - x} + \qopname \relax o{arcsin}\frac {3 - 2x}{5} $ \\}{2}{section*.7}
\contentsline {paragraph}{(A)9\hskip 1em\relax 设$y=f(x)$的定义域为$[0,1]$，问下列复合函数的定义域是什么？}{2}{section*.8}
\contentsline {subparagraph}{$(1)\hskip 1em\relax f(x^2)$ \\}{2}{section*.9}
\contentsline {subparagraph}{$(2)\hskip 1em\relax f(\qopname \relax o{sin}x)$ \\}{2}{section*.10}
\contentsline {subparagraph}{$(3)\hskip 1em\relax f(x+a),(a > 0)$ \\}{2}{section*.11}
\contentsline {subparagraph}{$(4)\hskip 1em\relax f(x+a)-f(x-a),(a > 0)$ \\}{3}{section*.12}
\contentsline {paragraph}{(A)12 \hskip 1em\relax 设}{3}{section*.13}
\contentsline {paragraph}{(A)12\hskip 1em\relax 如图1-17，一球的半径为$r$，作外切于球的正圆锥，试将其体积表示为高的函数，并求此函数的定义域。\\ \\}{4}{section*.14}
\contentsline {paragraph}{(A)16\hskip 1em\relax 收音机每台售价为90元，成本为60元，厂方为鼓励销售商大量采购，决定凡是订购量超过100台以上的，每多订购1台，售价就降低1分，但最低价为每台75元。\\ (1)将每台的实际售价$p$表示为订购量$x$的函数。\\ (2)将厂方所获的利润L表示为订购量$x$的函数。\\ (3)某一商行订购了1000台，厂方可获利润是多少？ }{4}{section*.15}
\contentsline {paragraph}{(B)1\hskip 1em\relax 试确定$f(x)=(2+\sqrt {3})^x+(2-\sqrt {3})^x$的奇偶性\\}{5}{section*.16}
\contentsline {paragraph}{设$\varphi (x),\psi (x)$及$f(x)$为单调增函数，证明若 \[ \varphi (x) \leq f(x) \leq \psi (x) \] 则 \[ \varphi (\varphi (x)) \leq f(f(x)) \leq \psi (\psi (x)) \] }{6}{section*.17}
\contentsline {paragraph}{(B)8\hskip 1em\relax 设$f(x)=$}{6}{section*.18}
\contentsline {subsection}{\numberline {1.1.2}习题1-2\hskip 1em\relax 数列的极限}{7}{subsection.1.1.2}
\contentsline {paragraph}{(A)3\hskip 1em\relax 设$x_n=\displaystyle \frac {\displaystyle \qopname \relax o{cos}\frac {n\pi }{2}}{n}$,问$\displaystyle \qopname \relax m{lim}_{n \rightarrow \infty } x_n ={} ?$求出N，当$n \geq N$时，使$x_n$与其极限之差的绝对值小于正数$\varepsilon $\\}{7}{section*.19}
\contentsline {paragraph}{(A)4\hskip 1em\relax 根据数列极限的定义证明：}{8}{section*.20}
\contentsline {subparagraph}{(2)\hskip 1em\relax $\displaystyle \qopname \relax m{lim}_{n \rightarrow \infty } \frac {3n+1}{2n+1} = \frac {3}{2}$\\}{8}{section*.21}
\contentsline {subparagraph}{(3)$\displaystyle \qopname \relax m{lim}_{n \rightarrow \infty } \frac {\sqrt {n^2 + a^2}}{n} = 1$\\}{8}{section*.22}
\contentsline {paragraph}{(A)5\hskip 1em\relax 若$\displaystyle \qopname \relax m{lim}_{n \rightarrow \infty } x_n = a$，证明$\displaystyle \qopname \relax m{lim}_{n \rightarrow \infty } |x_n| = |a|$，并举例说明反过来未必成立\\}{8}{section*.23}
\contentsline {paragraph}{(B)2\hskip 1em\relax 若$x_n \leq qx_{n-1}$，若$x_{n} > 0, 0 < q < 1$,求证$\displaystyle \qopname \relax m{lim}_{n \rightarrow \infty } x_n = 0$ \leavevmode {\color {red}(题目有改动)}\\}{9}{section*.24}
\contentsline {subsection}{\numberline {1.1.3}习题1-3\hskip 1em\relax 函数的极限}{9}{subsection.1.1.3}
\contentsline {paragraph}{(A)1\hskip 1em\relax 根据函数极限的定义证明：}{9}{section*.25}
\contentsline {subparagraph}{(2)\hskip 1em\relax $\displaystyle \qopname \relax m{lim}_{x \rightarrow -\displaystyle \frac {1}{2}} \frac {1-4x^2}{2x+1} = 2$\\}{9}{section*.26}
\contentsline {subparagraph}{(3)\hskip 1em\relax $\displaystyle \qopname \relax m{lim}_{x \rightarrow 4} \sqrt {x} = 2$\\}{9}{section*.27}
\contentsline {paragraph}{(A)2\hskip 1em\relax 根据函数极限的定义证明：}{10}{section*.28}
\contentsline {subparagraph}{(2)\hskip 1em\relax $\displaystyle \qopname \relax m{lim}_{x \rightarrow +\infty } \frac {\qopname \relax o{sin}x}{\sqrt {x}} = 0$\\}{10}{section*.29}
\contentsline {subparagraph}{(3)\hskip 1em\relax $\displaystyle \qopname \relax m{lim}_{x \rightarrow \infty } \frac {3x^2 - 1}{x^2 + 3} = 3$\\}{10}{section*.30}
\contentsline {paragraph}{(A)6\hskip 1em\relax 求$\displaystyle f(x) = \frac {x}{x},\varphi (x) = \frac {|x|}{x}$当$x \rightarrow 0$时的左、右极限，并说明它们在$\displaystyle x \rightarrow 0$时，极限是否存在？\\}{10}{section*.31}
\contentsline {paragraph}{(B)3\hskip 1em\relax 根据极限定义证明：函数$f(x)$当$x \rightarrow x_0$时极限存在的充分必要条件时左极限、右极限均存在并且相等\\}{11}{section*.32}
\contentsline {subsection}{\numberline {1.1.4}习题1-4\hskip 1em\relax 无穷大与无穷小}{12}{subsection.1.1.4}
\contentsline {paragraph}{(A)1\hskip 1em\relax 根据定义证明：}{12}{section*.33}
\contentsline {subparagraph}{(1) $\displaystyle y = \frac {x}{1+x}$当$x \rightarrow 0$时为无穷小\\}{12}{section*.34}
\contentsline {paragraph}{(A)2\hskip 1em\relax 根据定义证明：}{12}{section*.35}
\contentsline {subparagraph}{当$x \rightarrow 3$时，$\displaystyle \frac {x}{x-3} 为无穷大$\\}{12}{section*.36}
\contentsline {paragraph}{(A)4\hskip 1em\relax 问函数$\displaystyle y = \frac {x+1}{x-1}$在什么时候为无穷小？什么时候为无穷大？\\}{13}{section*.37}
\contentsline {paragraph}{(B)3\hskip 1em\relax 函数$y = xcosx$在$(-\infty , +\infty )$内是否有界？这个函数是否为$x \rightarrow +\infty $时的无穷大？为什么？\\}{13}{section*.38}
\contentsline {subsection}{\numberline {1.1.5}习题1-5\hskip 1em\relax 极限运算法则}{13}{subsection.1.1.5}
\contentsline {paragraph}{(A)2\hskip 1em\relax 计算下列极限}{13}{section*.39}
\contentsline {subparagraph}{(4)\hskip 1em\relax $\displaystyle \qopname \relax m{lim}_{x \rightarrow \infty } \frac {(x-1)(x-2)(x-3)}{(1-4x)^3}$\\}{13}{section*.40}
\contentsline {paragraph}{(A)3\hskip 1em\relax 计算下列极限}{13}{section*.41}
\contentsline {subparagraph}{(3)\hskip 1em\relax $\displaystyle \qopname \relax m{lim}_{n \rightarrow \infty } \frac {1+\frac {1}{2}+\frac {1}{4}+\dots +\frac {1}{2^n}}{1+\frac {1}{3}+\frac {1}{9}+\dots +\frac {1}{3^n}}$\\}{14}{section*.42}
\contentsline {paragraph}{(A)4\hskip 1em\relax 计算下列极限}{14}{section*.43}
\contentsline {subparagraph}{(4)\hskip 1em\relax $\displaystyle \frac {\sqrt [3]{n^2} \qopname \relax o{sin}n!}{n + 1}$\\}{14}{section*.44}
\contentsline {subparagraph}{(6)\hskip 1em\relax $\displaystyle \qopname \relax m{lim}_{n \rightarrow \infty } (1-\frac {1}{2^2})(1-\frac {1}{3^2})\dots (1-\frac {1}{n^2})$\\}{14}{section*.45}
\contentsline {paragraph}{(B)1\hskip 1em\relax 计算下列极限}{15}{section*.46}
\contentsline {subparagraph}{(7)\hskip 1em\relax $\displaystyle \qopname \relax m{lim}_{n \rightarrow \infty } (\sqrt {n+3\sqrt {n}}-\sqrt {n-\sqrt {n}})$\\}{15}{section*.47}
\contentsline {subsection}{\numberline {1.1.6}习题1-6\hskip 1em\relax 极限存在准则、两个重要极限}{15}{subsection.1.1.6}
\contentsline {paragraph}{(A)1\hskip 1em\relax 计算下列极限}{15}{section*.48}
\contentsline {subparagraph}{(6)\hskip 1em\relax $\displaystyle \qopname \relax m{lim}_{x \rightarrow 1} \frac {1 - x^2}{\qopname \relax o{sin}n\pi }$\\}{16}{section*.49}
\contentsline {paragraph}{(B)1\hskip 1em\relax 计算下列极限}{16}{section*.50}
\contentsline {subparagraph}{(7) $\displaystyle \qopname \relax m{lim}_{x \rightarrow \frac {\pi }{4}} (\qopname \relax o{tan}x)^{\qopname \relax o{tan}2x}$\\}{16}{section*.51}
\contentsline {paragraph}{(B)2\hskip 1em\relax 利用极限存在准则}{16}{section*.52}
\contentsline {subparagraph}{(2)证明：$\displaystyle \qopname \relax m{lim}_{n \rightarrow \infty } (\frac {1}{n^2+n+1} + \frac {2}{n^2+n+2} + \dots + \frac {n}{n^2+n+n}) = \frac {1}{2}$\\}{17}{section*.53}
\contentsline {subparagraph}{(5)已知$\displaystyle x_1=1,x_2=1+\frac {x_1}{1+x_1},\dots ,x_n=1+\frac {x_{n-1}}{1+x_{n-1}},\dots ,$证明数列$ \{x_n\} $极限存在，并求其极限值\\}{17}{section*.54}
\contentsline {subsection}{\numberline {1.1.7}习题1-9\hskip 1em\relax 连续函数的运算与初等函数的连续性}{18}{subsection.1.1.7}
\contentsline {paragraph}{(A)2\hskip 1em\relax 求\\}{18}{section*.55}
\contentsline {subsection}{\numberline {1.1.8}习题1-10\hskip 1em\relax 闭区间上连续函数的性质}{19}{subsection.1.1.8}
\contentsline {paragraph}{(A)2\hskip 1em\relax 证明方程$x=a\qopname \relax o{sin}x+b(a>0,b>0)$至少有一个正根，并且它不超过$a+b$\\}{19}{section*.56}
\contentsline {paragraph}{(A)4\hskip 1em\relax 设$f(x)$在$[a,b]$上连续，$a<x_1<x_2<\dots <x_n<b$，证明在$[x_1,x_n]$上必有$\xi $，使$\displaystyle f(\xi ) = \frac {1}{n} \DOTSB \sum@ \slimits@ _{i=1}^{n}f(x_i)$\leavevmode {\color {red}(题目有改动)}\\}{19}{section*.57}
\contentsline {paragraph}{(A)5\hskip 1em\relax 设$f(x)$在$[a,b]$上连续，且$f$的值域$f([a,b])\in [a,b]$，证明至少存在一点$\xi \in [a,b]$使得$f(\xi )=\xi $成立\leavevmode {\color {red}(题目有改动)}\\}{20}{section*.58}
\contentsline {paragraph}{B(1)\hskip 1em\relax 设$f(x)$在$(-\infty , +\infty )$内连续，且$\displaystyle \qopname \relax m{lim}_{x \rightarrow \infty } f(x) = A$，试证：$f(x)$在$(-\infty , +\infty )$内有界\\}{20}{section*.59}
\contentsline {paragraph}{B(2)\hskip 1em\relax 设$f(x)$在$[a,b]$上连续，且$a<c<d<b$，试证：在$[a,b]$上至少存在一点$\xi $，使得对任意整数$m,n$成立 \[ mf(c)+nf(d)=(m+n)f(\xi ) \] }{20}{section*.60}
\contentsline {section}{\numberline {1.2}第二章\hskip 1em\relax 导数与微分}{21}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}习题2-1\hskip 1em\relax 导数概念}{21}{subsection.1.2.1}
\contentsline {paragraph}{(A)9\hskip 1em\relax 讨论下列函数在点$x=0$处的连续性与可导性}{21}{section*.61}
\contentsline {subparagraph}{(2)\hskip 1em\relax }{21}{section*.62}
\contentsline {paragraph}{(A)12\hskip 1em\relax 设函数}{21}{section*.63}
\contentsline {paragraph}{(B)1\hskip 1em\relax 设$f(x) = (x-a) \varphi (x)$，而$\varphi (x)$在点$x=a$处连续，证明$f(x)$在点$x=a$处导数存在且等于$\varphi $(a)\\}{22}{section*.64}
\contentsline {paragraph}{(B)3\hskip 1em\relax 按定义证明：可导的偶函数的导数是奇函数，而可导的奇函数的导数是偶函数\\}{22}{section*.65}
\contentsline {paragraph}{(B)9\hskip 1em\relax 设$F(x) = \qopname \relax m{max}\{f_1(x),f_2(x)\},0<x<2$，其中$f_1(x)=x,f_2(x)=x^2$，求$F'(x)$\\}{23}{section*.66}
\contentsline {subsection}{\numberline {1.2.2}习题2-2\hskip 1em\relax 函数的求导法则}{24}{subsection.1.2.2}
\contentsline {paragraph}{(B)5\hskip 1em\relax 证明：双曲线$xy=a^2$上任一点处的切线与两坐标轴构成的三角形的面积都等于$2a^2$\\}{24}{section*.67}
\contentsline {paragraph}{(B)11\hskip 1em\relax 设$f(x)=a_1\qopname \relax o{sin}x + a_2\qopname \relax o{sin}2x+\dots +a_n\qopname \relax o{sin}nx$，并且$|f(x)| \leq |\qopname \relax o{sin}x|,a_1,a_2,\dots ,a_n$为常数，证明$|a_1+2a_2+\dots +na_n| \leq 1$\\}{25}{section*.68}
\contentsline {subsection}{\numberline {1.2.3}习题2-3\hskip 1em\relax 高阶导数}{25}{subsection.1.2.3}
\contentsline {paragraph}{(B)7\hskip 1em\relax 设$x=e^{-t}$，变换方程$\displaystyle x^2\frac {d^2y}{dx^2} + x\frac {dy}{dx} + y = 0$\\}{25}{section*.69}
\contentsline {subsection}{\numberline {1.2.4}习题2-5\hskip 1em\relax 函数的微分}{26}{subsection.1.2.4}
\contentsline {paragraph}{(A)3\hskip 1em\relax 求下列函数的微分}{26}{section*.70}
\contentsline {subparagraph}{(6)\hskip 1em\relax $y=\qopname \relax o{tan}^2 (1+2x^2)$\textbf {\color {red}(答案有错)}\\}{26}{section*.71}
\contentsline {section}{\numberline {1.3}第三章\hskip 1em\relax 微分中值定理和导数的应用}{26}{section.1.3}
\contentsline {subsection}{\numberline {1.3.1}习题3-1\hskip 1em\relax 微分中值定理}{26}{subsection.1.3.1}
\contentsline {paragraph}{(A)5\hskip 1em\relax 试证方程$4ax^3 + 3bx^2 + 2cx = a + b + c$在$(0,1)$内至少有一个实根\\}{26}{section*.72}
\contentsline {paragraph}{(A)8\hskip 1em\relax 若$0<b \leq a$，证明$\displaystyle \frac {a-b}{a} \leq \qopname \relax o{ln}\frac {a}{b} \leq \frac {a-b}{b}$\\}{27}{section*.73}
\contentsline {paragraph}{(A)10\hskip 1em\relax 设函数$f(x)$在$[0,1]$上连续，在$(0,1)$内可导，且$f(0).0,f(\frac {1}{2})<0,f(1)>0$，则在$(0,1)$内至少存在$\xi $，使$f'(\xi )=0$\\}{27}{section*.74}
\contentsline {paragraph}{(B)2\hskip 1em\relax 设$a_1,a_2,\dots ,a_n$是满足$\displaystyle a_1 - \frac {a_2}{3} + \dots + (-1)^{n-1}\frac {a_n}{2n-1}=0$的实数，证明方程$a_0 + a_1x + \dots + a_nx^n = 0$在$(0,1)$内至少有一实根\\}{28}{section*.75}
\contentsline {paragraph}{(B)4\hskip 1em\relax 若$f(x)$在$[a,b]$上连续，在$(a,b)$内二阶可导，且$f(a)=f(b)=0$及存在$c$，使$f(c)>0(a<c<b)$，证明：在$(a,b)$内必存在一点$\xi $，使$f''(\xi )<0$\\}{28}{section*.76}
\contentsline {paragraph}{(B)6\hskip 1em\relax 设$f(x)$在$[0,1]$上连续，在$(a,b)$内二阶可导，且$f(0)=f(1)=0$，证明：在$(0,1)$内必存在$\xi $，使$\displaystyle f''(\xi )=\frac {2f'(\xi )}{1-\xi }$\\}{29}{section*.77}
\contentsline {paragraph}{(B)11\hskip 1em\relax 设函数$f(x)$在$[a,+\infty )$上连续，并在$(a,+\infty )$内可导，且$f'(x)>k$(其中$k>0$)，若$f(a)<0$，若$f(a)<0$，试证$f(x)=0$在$\displaystyle (a,a-\frac {f(a)}{k})$内有唯一实根\\}{29}{section*.78}
\contentsline {subsection}{\numberline {1.3.2}习题3-2\hskip 1em\relax 洛必达法则}{30}{subsection.1.3.2}
\contentsline {paragraph}{(B)2\hskip 1em\relax 讨论函数}{30}{section*.79}
\contentsline {subsection}{\numberline {1.3.3}习题3-4\hskip 1em\relax 函数的单调性与凸性的判别法}{30}{subsection.1.3.3}
\contentsline {paragraph}{(A)4\hskip 1em\relax 证明下列不等式}{30}{section*.80}
\contentsline {subparagraph}{(3)\hskip 1em\relax 当$\displaystyle 0<x<\frac {\pi }{2}$时，$\displaystyle \qopname \relax o{tan}x > x + \frac {1}{3} x^3$\\}{31}{section*.81}
\contentsline {subparagraph}{(4)\hskip 1em\relax 当$x>4$时，$2^x>x^2$\\}{31}{section*.82}
\contentsline {paragraph}{(A)5\hskip 1em\relax 证明方程$\qopname \relax o{cos}x = x$在$(-\infty ,+\infty )$上只有一个实根\\}{31}{section*.83}
\contentsline {paragraph}{(B)6\hskip 1em\relax 若$f(0)=0$，且$f'(x)$在$[0,+\infty )$上单调递增，证明$\displaystyle \frac {f(x)}{x}$在$(0,+\infty )$内也是单调递增\\}{32}{section*.84}
\contentsline {paragraph}{(B)11\hskip 1em\relax 试证明曲线$\displaystyle y = \frac {x-1}{x^2 + 1}$有三个拐点位于同一直线上\\}{32}{section*.85}
\contentsline {subsection}{\numberline {1.3.4}习题3-6\hskip 1em\relax 函数图像的描绘}{33}{subsection.1.3.4}
\contentsline {paragraph}{(A)2\hskip 1em\relax 描绘下列函数图像的渐近线}{33}{section*.86}
\contentsline {subparagraph}{(2)\hskip 1em\relax $\displaystyle y = \frac {x}{1+x^2}$\\}{33}{section*.87}
\contentsline {section}{\numberline {1.4}第四章\hskip 1em\relax 不定积分}{34}{section.1.4}
\contentsline {subsection}{\numberline {1.4.1}习题4-1\hskip 1em\relax 不定积分的概念与性质}{34}{subsection.1.4.1}
\contentsline {paragraph}{(A)1\hskip 1em\relax 求下列不定积分}{34}{section*.88}
\contentsline {subparagraph}{(8)$\displaystyle \DOTSI \intop \ilimits@ 2^{-x} e^x dx$ \leavevmode {\color {red}(答案有错)}\\}{34}{section*.89}
\contentsline {subsection}{\numberline {1.4.2}习题4-3\hskip 1em\relax 分部积分法}{34}{subsection.1.4.2}
\contentsline {paragraph}{(B)1\hskip 1em\relax 求下列不定积分}{34}{section*.90}
\contentsline {subparagraph}{(7)\hskip 1em\relax $\displaystyle \DOTSI \intop \ilimits@ x \qopname \relax o{arcsin}x dx$\leavevmode {\color {red}(答案有错)}\\}{34}{section*.91}
