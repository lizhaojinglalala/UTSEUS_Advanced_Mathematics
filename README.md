# 中欧高数答案

#### 项目介绍

一份中欧的高数答案

#### 进度介绍

v(章节数).(小节数，占两位)(版号数)
例：v2.013 目前进度已至第二章第一节（第三次提交）

#### 版权许可

本作品采用知识共享 署名-非商业性使用 4.0 国际 许可协议进行许可。要查看该许可协议，可访问 http://creativecommons.org/licenses/by-nc/4.0/ 或者写信到 Creative Commons, PO Box 1866, Mountain View, CA 94042, USA。

#### 使用说明

见前言

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request